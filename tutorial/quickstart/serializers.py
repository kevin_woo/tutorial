# -*- coding:utf-8 -*-

"""
@version: python3.6.3
@author: ken.wu
@license: BSD Licence 
@contact: code4fs@gmail.com
@software: PyCharm
@file: serializers.py
@time: 2018/10/6 00:05
"""

from rest_framework import serializers
from .models import Snippet, LANGUAGE_CHOICES, STYLE_CHOICES
from django.contrib.auth.models import User


# class SnippetSerializers(serializers.Serializer):
#     """
#     为我们的WEBAPI提供一种将代码片段实例序列化和反序列化为诸如Json之类的表示形式
#     """
#     id = serializers.IntegerField(read_only=True)
#     title = serializers.CharField(required=False, allow_blank=True, max_length=100)
#     code = serializers.CharField(style={'base_template': 'textarea.html'})
#     linenos = serializers.BooleanField(required=False)
#     language = serializers.ChoiceField(choices=LANGUAGE_CHOICES, default='python')
#     style = serializers.ChoiceField(choices=STYLE_CHOICES, default='friendly')
#
#     def create(self, validated_data):
#         """
#         根据提供的验证过的数据创建并返回一个新的snippet实例
#         :param validated_data:
#         :return:
#         """
#         return Snippet.objects.create(**validated_data)
#
#     def update(self, instance, validated_data):
#         """
#         根据提供的验证过的数据更新和返回一个已经存在的snippet实例
#         :param instance:
#         :param validated_data:
#         :return:
#         """
#         instance.title = validated_data.get('title', instance.title)
#         instance.code = validated_data.get('code', instance.code)
#         instance.linenos = validated_data.get('linenos', instance.linenos)
#         instance.language = validated_data.get('language', instance.language)
#         instance.style = validated_data.get('style', instance.style)
#         instance.save()
#         return instance

# class SnippetSerializer(serializers.ModelSerializer):
#     """
#     使用ModerSerializer类
#     """
#     owner = serializers.ReadOnlyField(source='owner.username')
#     class Meta:
#         model = Snippet
#         fields = ('id', 'title', 'code', 'linenos', 'language', 'style', 'owner')


class SnippetSerializer(serializers.HyperlinkedModelSerializer):
    """
    使用ModerSerializer类
    """
    owner = serializers.ReadOnlyField(source='owner.username')
    # url = serializers.HyperlinkedIdentityField(view_name='qk:snippet-detail', lookup_field='pk')
    url = serializers.HyperlinkedIdentityField(view_name='snippet-detail', lookup_field='pk')

    # 视图函数名：views_name,后缀为html，而不是json，因为我们需要的就是高亮的文本

    # highlight = serializers.HyperlinkedIdentityField(view_name='qk:snippet-highlight', format='html')
    highlight = serializers.HyperlinkedIdentityField(view_name='snippet-highlight', format='html')


    class Meta:
        model = Snippet
        fields = ('url', 'id', 'highlight', 'owner', 'title', 'code', 'linenos', 'language', 'style')


# class UserSerializer(serializers.ModelSerializer):
#     snippets = serializers.PrimaryKeyRelatedField(many=True, queryset=Snippet.objects.all())
#
#     class Meta:
#         model = User
#         fields = ('id', 'username', 'snippets')

class UserSerializer(serializers.HyperlinkedModelSerializer):
    # snippets = serializers.PrimaryKeyRelatedField(many=True, queryset=Snippet.objects.all())
    # url = serializers.HyperlinkedIdentityField(view_name='qk:user-detail', lookup_field='pk')
    # snippets = serializers.HyperlinkedRelatedField(many=True, view_name='qk:snippet-detail', read_only=True)
    url = serializers.HyperlinkedIdentityField(view_name='user-detail', lookup_field='pk')
    snippets = serializers.HyperlinkedRelatedField(many=True, view_name='snippet-detail', read_only=True)

    class Meta:
        model = User
        fields = ('url', 'id', 'username', 'snippets')
