# -*- coding:utf-8 -*-

"""
@version: python3.6.3
@author: ken.wu
@license: BSD Licence 
@contact: code4fs@gmail.com
@software: PyCharm
@file: permissions.py
@time: 2018/10/10 01:43
"""

from rest_framework import permissions


class IsOwnerOrReadOnly(permissions.BasePermission):
    """
    自定义权限只允许对象的所有者编辑它
    """

    def has_object_permission(self, request, view, obj):
        """
        # 读取权限允许任何请求
        # 所以我们总是允许GET,HEAD或OPTIONS请求
        :param request:
        :param view:
        :param obj:
        :return:
        """
        if request.method in permissions.SAFE_METHODS:
            return True

            # 只有该snippet的所有者才允许写权限
        return obj.owner == request.user