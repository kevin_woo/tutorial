# -*- coding:utf-8 -*-

"""
@version: python3.6.3
@author: ken.wu
@license: BSD Licence 
@contact: code4fs@gmail.com
@software: PyCharm
@file: urls.py
@time: 2018/10/7 00:42
"""

from django.conf.urls import url
import quickstart.views as views
# from rest_framework.urlpatterns import format_suffix_patterns
from rest_framework import renderers


app_name = 'qk'


snippet_list = views.SnippetViewSet.as_view({
    'get': 'list',
    'post': 'create'
})

snippet_detail = views.SnippetViewSet.as_view({
    'get': 'retrieve',
    'put': 'update',
    'patch': 'partial_update',
    'delete': 'destroy'
})

snippet_highlight = views.SnippetViewSet.as_view({
    'get': 'highlight'
}, renderer_classes=[renderers.StaticHTMLRenderer])

user_list = views.UserViewSet.as_view({
    'get': 'list'
})

user_detail = views.UserViewSet.as_view({
    'get': 'retrieve'
})

urlpatterns = [
    # url(r'api/$', views.api_root),
    # url(r'snippets/$', views.snippet_list),
    # url(r'snippets/(?P<pk>[0-9]+)/$', views.snippet_detail),
    # url(r'^index/$',views.IndexView.as_view()),
    # url(r'snippets/$', views.SnippetList.as_view(), name='snippet-list'),
    # url(r'snippets/(?P<pk>[0-9]+)/$', views.SnippetDetail.as_view(), name='snippet-detail'),
    # url(r'users/$', views.UserList.as_view(), name='user-list'),
    # url(r'users/(?P<pk>[0-9]+)/$', views.UserDetail.as_view(), name='user-detail'),
    #
    # url(r'snippets/(?P<pk>[0-9]+)/highlight/$', views.SnippetHighlight.as_view(), name='snippet-highlight'),

    url(r'snippets/$',snippet_list, name='snippet-list'),
    url(r'snippets/(?P<pk>[0-9]+)/$', snippet_detail, name='snippet-detail'),
    url(r'snippets/(?P<pk>[0-9]+)/highlight/$', snippet_highlight, name='snippet-highlight'),

    url(r'users/$', user_list, name='user-list'),
    url(r'users/(?P<pk>[0-9]+)/$', user_detail, name='user-detail'),



]
# urlpatterns = format_suffix_patterns(urlpatterns)
